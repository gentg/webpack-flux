'use strict';

var webpack = require('webpack'),  
  config = require('./sharedSrc/config/config'),
  path = require('path'),
  srcPath = path.join(__dirname, 'clientSrc'),
  ExtractTextPlugin = require('extract-text-webpack-plugin'),
  testPath = path.join(__dirname, '/test'),
  publicPath = config.siteUrl+"/public";

module.exports = {  
  
  target: 'web',
  cache: true,
  entry: {
    bundle: path.resolve(srcPath, 'module.js'), 
    tests: path.resolve(testPath,'tests.js'),
    common:['react', 'react-router', 'alt']
  },
  output: {
    path: path.join(__dirname, 'public'),
    publicPath: publicPath,
    filename: '[name].js'
  },

  module: {
    loaders: [
      {test: /\.spec.js$/, loader: "mocha"},
      {test: /\.js$/, exclude: /node_modules/, loader: 'babel?cacheDirectory'},
      {test: /\.sass$/, loader: ExtractTextPlugin.extract("css!sass?indentedSyntax")},
      {test: /\.css$/, loader: ExtractTextPlugin.extract("css")},
      {test: /\.woff$|\.woff2$/,   loader: "url?limit=10000&minetype=application/font-woff"},
      {test: /\.ttf$|\.eot$|\.svg$/,    loader: "file"},
    ]
  },
  plugins: [
   new webpack.optimize.CommonsChunkPlugin('common', 'common.js'),
   new ExtractTextPlugin('assets/css/styles.css',{
    allChunks: true
   }),
   new webpack.NoErrorsPlugin()
  ],

  debug: true,
  devtool: 'eval-cheap-module-source-map',
  devServer: {
    contentBase: '/public',
    historyApiFallback: true
  }
};