//load css assets with webpack
import "../node_modules/bootstrap/dist/css/bootstrap.css";
import "../public/assets/sass/style.sass";

import React from 'react';
import Router from 'react-router';
import routes from '../sharedSrc/routes';
import Iso from 'iso';
import flux from '../sharedSrc/flux';


Iso.bootstrap(function (state, meta, container) {
	Router.run(routes, Router.HistoryLocation, (Handler) => {
		flux.bootstrap(state);
		React.render(
			<Handler flux={flux}/>,
			container
		);
	});
});
