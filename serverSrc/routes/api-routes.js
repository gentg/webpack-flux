
var routes = (app) => {
	app.get('/api/users', (req, res) => {
		res.json({hello:"world"});
	});


	app.get('/api/*', (req, res) => {
		res.json({error: "no such API endpoint exists!"});
	});
}

export default routes;