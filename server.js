import express from 'express';
import bodyParser from 'body-parser';
import path from 'path';
import nodeJSX from 'node-jsx';
import React from 'react';
import Router from 'react-router';
import apiRoutes from './serverSrc/routes/api-routes.js';
import reactRoutes from './sharedSrc/routes';
import flux from './sharedSrc/flux';
import Iso from 'iso';
import config from './sharedSrc/config/config';
import util from 'util';

nodeJSX.install();

let app = express();

let port = 9090;

let loadConfig = (req, res, next) => {
	res.locals.config = config;
	next();
} 

app.use(loadConfig);

app.use(bodyParser.urlencoded({extended:true}));

app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'public')));

app.set('views', path.join(__dirname, 'public'));

app.set('view engine', 'ejs');

apiRoutes(app);


app.get('/tests', (req, res) => {
  res.render('test');
});

app.get('/*', (req, res, next) => {
  Router.run(reactRoutes, req.url, (Handler, state) => {
  	if(state.pathname == undefined){
  		res.redirect('/error');
  	}else{
    	let component = React.renderToString(<Handler flux={flux}/>);
    	let content = Iso.render(component, flux.flush());

    	res.render('index', { content: content });
  	}
  });
});


app.listen(port);

console.log("Server is Up and Running at Port : " + port);