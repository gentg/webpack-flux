Flux-React-Express-Webpack Isomorphic example
---------

An example showing how to efficiently start building Isomorphic applications using Flux architechture. ES2015/ES7 standard is used for the JavaScript files. Babel is used for transpilation. The code is fairly straight forward and easy to understand.

######To start the app run the following commands from the root folder:
- npm install -g webpack(if you do not have webpack globally installed) 
- npm install -g webpack-dev-server
- npm install(To install node dependencies as specified in package.json) 
- npm run build
- npm run watch
- node app.js
- Go to localhost:9090

####Technologies/Libraries used

- React.js
- Alt.js (for flux)
- immutable.js (immutable data structures for better reactjs performance)
- iso.js (helps with isomorphic approach)
- Express.js
- Ejs (Templating)
- Webpack

#####For Testing

- Jest
- Mocha
- Sinon
- Chai

####Short Structure Description

Most of the code is located in three folders serverSrc, sharedSrc and serverSrc. The names are self-explanatory, code used only by the client is located in clientSrc(starting point of our app in the browser where our app is 'bootstrapped'), code used by the server(Express api routes...etc) in in serverSrc, and since our app is 'isomorphic' and React.js can run both on server and client, in sharedSrc there is code that is shared between client and server(React components... etc). Isomorphic applications have a lot of advantages, among them shared code, better SEO and better 'perceived' load times since the first page render is done by the server and then the client takes over where the 'server left off'. Iso.js is useful here because it helps to preserve the state of the application between client and server. As mentioned above everything is written in ES6/ES7 syntax and webpack uses Babel to transpile the code into ES5 syntax.

#####Webpack

Webpack is used for module bundling and some task management like transpiling Javascript files and tests with the help of Babel, precompiling sass into css etc. Effectively replaces Gulp or Grunt for many things. Extremely helpful. webpack dev server also does the "hot reloading" and loads the necessary assets into the app. Both webpack dev server and the express server have to be activated in order for the example to work properly. Once webpack does its job you code should be bundled in the 'public' directory.

######npm commands

- npm run build (Run a webpack command to build the necessary modules and dependencies)
- npm run watch (Run a webpack command to  watch for changes and rebuilds only what has changed and then reloads the browser)
- npm test (run Jest tests)
- npm deploy (Run a webpack command to Build the app for deployment)

#####Express

Express 4 is used server-side because is a proven and lightweight(ish) framework for nodejs. You can add only the middleware you require for your project.

#####Alt

Alt.js is the flux implementation of choice here. It has great support for ES6/ES7 including decorators and spares you from a lot of boilerplate code you would otherwise have to write yourself. In my opinion it is the best implementation of flux for the moment and it is going the right way for sure. 

#####React

React.js is used to build the UI because it goes perfectly with Flux(recommended by Facebook), it is very performant(more so when used with immutable.js) and it is 'component-oriented' which in my opinion is the best way to build apps right now because it has so many advantages.(https://facebook.github.io/react/index.html) 

#####Testing

Jest and Mocha are both used in this example just to demonstrate how both can be used to test flux-react applications. Jest will have to be run from the command line. Mocha tests can run in the browser just navigate to "localhost:9090/test".