import React from 'react';
import {Route} from 'react-router';
import Main from '../sharedSrc/components/main';
import Home from '../sharedSrc/components/home';
import ErrorPage from '../sharedSrc/components/errorPage';

const routes = (
	<Route name="main" path="/" handler={Main}>
		<Route name="home" handler={Home} />
		<Route name="error" handler={ErrorPage} />
		<Route path="/favicon.ico" />
	</Route>
);

export default routes;
