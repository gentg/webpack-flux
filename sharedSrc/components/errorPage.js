import React from 'react';

class ErrorPage extends React.Component{

	constructor(props){
		super(props);
	}

	render(){
		return (
			<div>
				<h1>{this.props.message}</h1>
			</div>
		);
	}
}

ErrorPage.defaultProps = {message: "Default error message!"};

export default ErrorPage;