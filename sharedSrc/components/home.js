
import React from 'react';  
import connectToStores from 'alt/utils/connectToStores';  
import FirstStore from '../stores/firstStore';  
import SecondStore from '../stores/secondStore';  
import Actions from '../actions/actions';
import ShowInfo from './showInfo';
import UsersList from './usersList';


@connectToStores
class Example extends React.Component {  

  constructor(props) {
    super(props);
  }

  static getStores() {
    return [FirstStore, SecondStore];
  }

  static getPropsFromStores() {
    return {
      name: FirstStore.getState().get('name'),
      lastname: SecondStore.getState().get('lastname'),
      visible: SecondStore.getState().get('visibleList'),
      users: FirstStore.getState().get('users'),
      errorMsg: SecondStore.getState().get("errorMsg")
    };
  }

  static propTypes = {
    name: React.PropTypes.string.isRequired,
    lastname: React.PropTypes.string.isRequired
  }

  render() {
    return (
      <div className="example-page">
        <div className="top-div">
        <div className="form-group">
            <label>Name: </label>
            <input className="form-control" ref="name" type="text" onKeyPress={this.checkIfEnterPressed}  onChange={this.onChangeName} value={this.props.name} placeholder="Name..."/>
            <label>Lastname: </label>
            <input className="form-control" ref="lastname" type="text" onKeyPress={this.checkIfEnterPressed} onChange={this.onChangeLastname} value={this.props.lastname} placeholder="Lastname..." />
            <button className="btn btn-primary add-name" onClick={this.addUserToList}>Add To List</button>
          </div>
          <ShowInfo name={this.props.name} lastname={this.props.lastname}/>
        </div>
        <div className={`alert alert-danger ${this.props.errorMsg == ''? 'hidden' : '' }`} role="alert">
          <span className="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
          <span className="sr-only">Error: </span>
          {'  '+this.props.errorMsg}
        </div>
        <div className="middle-div">
        <UsersList visible={this.props.visible} users={this.props.users}/>
        </div>
      </div>
    );
  }

  checkIfEnterPressed = ev => {
    if(ev.key === 'Enter'){
      this.addUserToList();
    }
  }

  onChangeName = ev => {
    Actions.updateName(ev.target.value);
  }

  onChangeLastname = ev => {
    Actions.updateLastname(ev.target.value);
  }

  addUserToList = () => {
    let name = React.findDOMNode(this.refs.name).value;
    let lastname = React.findDOMNode(this.refs.lastname).value;
    if(name && lastname){
      Actions.addUserToList({name, lastname});
    }else{
      Actions.setErrorMsg("Please fill in all the fields!");
    }

    React.findDOMNode(this.refs.name).focus();
  }
}

export default Example;  