import React from 'react';
import Actions from '../actions/actions';

class UsersList extends React.Component{
	
	constructor(props){
		super(props);
	}

	shouldComponentUpdate(nextProps, nextState){
    	return this.props.users !== nextProps.users;
  	}
 
	render(){
		let users = this.props.users.map((user,index) => {
					return (
						<li className="list-group-item names-li" key={user.id}>
							<span>Name: <strong>{user.name}</strong>, Lastname: <strong>{user.lastname}</strong></span>
							<span className="badge"><button className="btn btn-danger btn-xs" onClick={this.removeUser.bind(this,user)}>DELETE</button></span>
						</li>
					);
				});

		return (
			<div className={`users-list-div ${this.props.visible==false ? 'hidden' : ''}`}>
				<h3>Users List</h3>
				<ul className="list-group">
				  {users}
				</ul>
			</div>
		);
	}

	removeUser = (user, ev) => {
		Actions.removeUserFromList(user);
	}
}


export default UsersList;