//jest.autoMockOff();
jest.dontMock('../showInfo');

import React from 'react/addons';

//we use require here because at the time of this writing jest does not support es2015 modules yet
const ShowInfo = require('../showInfo');
var TestUtils = React.addons.TestUtils;

describe('ShowInfo', () => {

 	it('renders properly', () => {
	    var showInfo = TestUtils.renderIntoDocument(<ShowInfo name="Gent" lastname="Gashi" />);

	    var h3 = TestUtils.scryRenderedDOMComponentsWithTag(showInfo, 'h3')[0];
	    
	    expect(h3.getDOMNode().textContent).toEqual('Name: Gent');
	});
});