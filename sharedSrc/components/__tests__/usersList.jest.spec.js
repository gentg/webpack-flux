jest.dontMock('../../tests/userFixtures');
jest.dontMock('../usersList');

import React from 'react/addons';
var TestUtils = React.addons.TestUtils;
var userFixtures = require('../../tests/userFixtures');

describe("'UsersList' React Component", () => {
    

    var usersList;

    //just for demonstration purposes
    beforeEach(() => {
       var UsersList  = require('../usersList');
	   usersList = TestUtils.renderIntoDocument(<UsersList users={userFixtures} />);
    });

 	it('renders properly', () => {
       	expect(usersList).not.toEqual(undefined);
	});

    it('renders a ul element', () => {
        let ulElement = TestUtils.findRenderedDOMComponentWithTag(usersList, 'ul');
        expect(ulElement).not.toEqual(undefined);
    });

	it('renders li elements', () => {
        let usersFixturesLength = userFixtures.length;
        let liElements = TestUtils.scryRenderedDOMComponentsWithTag(usersList, 'li');
    	expect(liElements.length).toEqual(usersFixturesLength);
    });

});
