import React from 'react'; 

class ShowInfo extends React.Component{
	
	constructor(props){
		super(props); 
	}

	render(){
		return (
			<div>
				<h3>Name: {this.props.name}</h3>
				<h3>Lastname: {this.props.lastname}</h3>
			</div>
		);
	}
}


export default ShowInfo;