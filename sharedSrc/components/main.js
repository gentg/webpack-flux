import React from 'react';
import {RouteHandler, Link} from 'react-router';

class Main extends React.Component{

	render(){
		return (
			<div className="main">
				<div className="sample-page">
					<h1>Sample Page</h1>
					<Link to="main">Main</Link>
					<span>   |   </span>
					<Link to='home'>Form</Link>
				</div>
				<RouteHandler/>
			</div>
		);
	}
}

export default Main;