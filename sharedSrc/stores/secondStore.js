import flux from '../flux';
import {createStore, bind} from 'alt/utils/decorators';
import Actions from '../actions/actions'; 
import FirstStore from './firstStore';
import immutable from 'alt/utils/ImmutableUtil';
import Immutable from 'immutable';

@createStore(flux)
@immutable
class SecondStore{
	constructor(){
		this.state = Immutable.Map({
			lastname: '',
			errorMsg: '',
			visibleList: false
		});
	}
		
	@bind(Actions.updateLastname)
	updateLastname(lastname){
		this.setState(this.state.set('lastname', lastname));
	}

	@bind(Actions.addUserToList)
	updateVisible(user){
		this.waitFor(FirstStore.dispatchToken);
		let state = this.state.set('visibleList', FirstStore.getState().get('users').size > 0?true:false)
			.set('lastname', '')
			.set('errorMsg', '');

		this.setState(state);
	}

	@bind(Actions.setErrorMsg)
	setErrorMsg(message){
		this.setState(this.state.set('errorMsg', message));
	}

	@bind(Actions.removeUserFromList)
	afterUserRemoved(user){
		this.waitFor(FirstStore.dispatchToken);
		this.setState(this.setVisibleListState(FirstStore.getState().get('users')));
	}

	setVisibleListState(usersList){
		return this.state.set('visibleList', usersList.size > 0?true:false);
	}
}

export default SecondStore;
