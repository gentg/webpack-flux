jest.dontMock('../firstStore.js');

var Actions = require('../../actions/actions');

describe('FirstStore', () => {

  var flux;
  var callback;
  var FirstStore;

  beforeEach(() => {
    flux = require('../../flux');
    flux.dispatcher.register = jest.genMockFunction();
    FirstStore = require('../firstStore');
    callback = flux.dispatcher.register.mock.calls[0][0];
  });

  it('registers a callback with the dispatcher', () => {
    expect(flux.dispatcher.register.mock.calls.length).toBe(1);
  });

  it('sets the name property', () => {

    let testName = "Gent";

    var payload =  {
        action: Actions.UPDATE_NAME,
        data: testName
    };
    callback(payload);
    expect(FirstStore.getState().get('name')).toBe(testName);
  });
});