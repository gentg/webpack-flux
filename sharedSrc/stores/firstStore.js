import flux from '../flux';
import {createStore, bind} from 'alt/utils/decorators';
import Actions from '../actions/actions';
import immutable from 'alt/utils/ImmutableUtil';
import Immutable from 'immutable';
//import SecondStore from './secondStore';

@createStore(flux)
@immutable
class FirstStore{

	constructor(){
		this.state = Immutable.Map({
			name: "",
			users: []
		});
	}

	@bind(Actions.updateName)
	updateName(name){
		this.setState(this.state.set('name',name));
	}

	@bind(Actions.addUserToList)
	addUserToList(user){
		let userWithId = Object.assign({id: new Date().getTime()}, user);
		let state = this.state.set("users", this.state.get('users').push(userWithId))
			.set('name', '');
			
		this.setState(state);	
	}

	@bind(Actions.removeUserFromList)
	removeUserFromList(user){
		let users = this.state.get('users').filter((u) => {
			return user.id !== u.id; 
		});

		let state = this.state.set('users', users);

		this.setState(state);
	}
}

export default FirstStore;
