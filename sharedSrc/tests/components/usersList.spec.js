import {expect} from "chai";
import React from 'react/addons';
import UsersList  from '../../components/usersList';
import userFixtures from '../userFixtures';
var TestUtils = React.addons.TestUtils;

describe("'UsersList' React Component", () => {

    let usersList;

    //just for demonstration purposes
    beforeEach(() => {
	   usersList = TestUtils.renderIntoDocument(<UsersList users={userFixtures} />);
    });

 	it('renders properly', () => {
       	expect(usersList).to.not.equal(undefined);
	});

    it('renders a ul element', () => {
        let ulElement = TestUtils.findRenderedDOMComponentWithTag(usersList, 'ul');
        expect(ulElement).to.not.equal(undefined);
    });

	it('renders li elements', () => {
        let usersFixturesLength = userFixtures.length;
        let liElements = TestUtils.scryRenderedDOMComponentsWithTag(usersList, 'li');
    	expect(liElements.length).to.equal(usersFixturesLength);
    });

});
