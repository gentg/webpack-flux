import {expect} from "chai";
import React from 'react/addons';
import ShowInfo  from '../../components/showInfo';
var TestUtils = React.addons.TestUtils;

describe("'ShowInfo' React Component", () => {
	
	let showInfo = TestUtils.renderIntoDocument(<ShowInfo name="Gent" lastname="Gashi" />);
	    
 	it('renders properly', () => {
       	expect(showInfo).to.not.equal(undefined);
	});

	it('shows name', () => {
        let h3name = TestUtils.scryRenderedDOMComponentsWithTag(showInfo, 'h3')[0];
    	expect(h3name.getDOMNode().textContent).to.equal('Name: Gent');
    });

    it('shows lastname',() => {
        let h3lastname = TestUtils.scryRenderedDOMComponentsWithTag(showInfo, 'h3')[1];
	    expect(h3lastname.getDOMNode().textContent).to.equal('Lastname: Gashi');
    });
});
