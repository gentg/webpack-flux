let users = [
	{
		id:123,
		name: "Gent",
		lastname: "Gashi"
	},
	{
		id:32423,
		name: "Shkumbin",
		lastname: "Maksuti"
	},
	{
		id:23423,
		name: "Granit",
		lastname: "Hoda"
	},
	{
		id:16534,
		name: "Fidan",
		lastname: "Ademi"
	}
];

export default users;