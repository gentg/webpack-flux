import flux from '../../flux';
import Actions from '../../actions/actions';
import immutable from 'alt/utils/ImmutableUtil';
import Immutable from 'immutable';
import SecondStore from '../../stores/secondStore';
import FirstStore from '../../stores/firstStore';
import {expect} from 'chai';

describe("FirstStore", () => {

	afterEach(() => {
		flux.flush();
	});

	it("updates the 'name' property",() => {
		let testName = "Gent";
		Actions.updateName(testName);

		let name = FirstStore.getState().get('name');

		expect(name).to.equal(testName);
	});
});