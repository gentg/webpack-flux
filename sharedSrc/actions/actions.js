import flux from '../flux';
import {createActions} from 'alt/utils/decorators';

@createActions(flux)
class Actions{
	constructor(){
		this.generateActions(
			'updateName',
			'updateLastname',
			'addUserToList',
			'setErrorMsg',
			'removeUserFromList'
		);
	}

	// updateName(name){
	// 	this.dispatch(name);
	// }

	// updateLastname(lastname){
	// 	this.dispatch(lastname);
	// }

	// addUserToList(user){
	// 	this.dispatch(user);
	// }

	// setErrorMsg(message){
	// 	this.dispatch(message);
	// }

	// removeUserFromList(user){
	// 	this.dispatch(user);
	// }
}

export default Actions;
